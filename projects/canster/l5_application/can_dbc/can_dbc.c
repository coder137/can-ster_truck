#include "can_dbc.h"

#include "can_bus_constants.h"

/**
 * TODO, Stub functions
 * Add your own can code to these functions
 */

void can_dbc__manage_all_mia(void) {
  /**
   * ! DO NOT PUT `dbc_service_mia_xyz` HERE
   * 1. Make a module
   * 2. Make the `can_xyz__manage_mia` function there
   * 3. Call that function here
   */

  /**
   * Example of what the function `can_xyz__manage_mia` would look like
   * -> Make a callback function as well for debugging
   */
  // void can_handler__device_heartbeat_manage_mia(void (*handle_func)(dbc_DRIVER_HEARTBEAT_s)) {
  //   const uint32_t mia_increment_value = 1000;

  //   if (dbc_service_mia_DRIVER_HEARTBEAT(&driver_heartbeat, mia_increment_value)) {
  //     fprintf(stderr, "MIA -> DRIVER_HEARTBEAT\r\n");
  //     if (handle_func) {
  //       handle_func(driver_heartbeat);
  //     }
  //   }
  // }
}

void can_dbc__handle_all_incoming_messages(void) {
  can__msg_t recv_message;
  while (can__rx(CAN_PORT, &recv_message, 0)) {

    const dbc_message_header_t header = {
        .message_id = recv_message.msg_id,
        .message_dlc = recv_message.frame_fields.data_len,
    };

    (void)header; // TODO, Remove this when used

    /**
     * ! DO NOT PUT DECODE AND MESSAGE HERE DIRECTLY
     * 1. Make a module
     * 2. Pass `recv_message.data.bytes` and `header` to it
     * 3. Update the `static local variables inside that module`
     *
     *
     * EXAMPLE:
     * Say we are getting ultra sonic sensor data to the driver node
     * 1. Create a module called can_driver
     * 2. Make a function called can_driver__decode_message(params...);
     * 3. Put just that function here
     * 4. Pass the above data to that function
     * All the other states and information are to be managed by the can_driver module
     */
  }
}

void can_dbc__transmit_message_10hz(void) {
  /**
   * 1. Create your message -> `message_to_send`
   * 2. Use autogenerated function -> `dbc_encode_and_send_xyz(NULL, &message_to_send);`
   * IMPORTANT: Make sure the message creation and encode and send is also handled by a module(d) function
   * -> Take a look at the above two function examples as well
   */

  // Example inside a different module
  //   if (!dbc_encode_and_send_ACCEL_QUAD(NULL, &quad_message)) {
  // #if DEBUG
  //     printf("Failed to encode and send Accelerometer Quad Data\r\n");
  // #endif
  //   }
}

/**
 * EXTERN inside project.h
 */
bool dbc_send_can_message(void *argument_from_dbc_encode_and_send, uint32_t message_id, const uint8_t bytes[8],
                          uint8_t dlc) {
  // unused
  (void)argument_from_dbc_encode_and_send;

  can__msg_t can_send = {};

  can_send.frame = 0;
  can_send.frame_fields.data_len = dlc;
  can_send.frame_fields.is_29bit = 0;
  can_send.frame_fields.is_rtr = 0;

  can_send.msg_id = message_id;
  memcpy(can_send.data.bytes, bytes, dlc);

  bool sent = true;
  if (!can__tx(CAN_PORT, &can_send, 0)) {
#if DEBUG == 1
    fprintf(stderr, "[%s] Can Tx Error\r\n", __FUNCTION__);
#endif
    sent = false;
  }

  return sent;
}
